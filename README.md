# OpenShift V3 Workshop Labs

These labs are specific for the Operations team or any OpenShift Admin.

## Lab Exercises 

Standalone Labs

* [Managing Users Overview](managing_users_overview.md)
* [Assigning Users to Project Roles](assigning_users_to_project_roles.md)
* [Creating Custom Roles](creating_custom_roles.md)
* Assigning Limit Ranges and Quotas
* Setting up Default Limit Ranges and Quotas for Projects
* Limiting Number of Self-Provisioned Projects
* Installing and Using Cockpit
* [Deploying Cockpit as a container](deploying_cockpit_as_a_container.md)
* [Deploying Container Native Storage](cns.md)

## Extended Lab Exercises 

Labs that require additional services

* [Adding an LDAP Provider](adding_an_ldap_provider.md)
* Adding An Additional Node
